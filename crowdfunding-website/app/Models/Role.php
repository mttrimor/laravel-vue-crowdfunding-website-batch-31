<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Uuid;


class Role extends Model
{
    use HasFactory,Uuid;

    protected $fillable = ['name'];
    protected $primaryKey = 'id';    

    public function user(){
        return $this->hasMany('App\User');
    }
}
