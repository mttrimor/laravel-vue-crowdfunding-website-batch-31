<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use App\Traits\Uuid;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, Uuid;

        /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'username',
        'email',
        'password',
        'role_id'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $primaryKey = 'id';
    

    public function role(){
        return $this->belongsTo('App\Role');
    }

    public function otpcode(){
        return $this->hasOne('App\Otpcode');
    }


    public function isAdmin(){
        // if($this->role_id === $this->get_role_admin()) {
        //     return true;
        // }
        // return false;

        if($this->role){
            if($this->role->name === 'admin'){
                return true;
            }
        }
        return false;
    }

    public function get_role_admin(){
        $role = Role::where('name','admin')->first();
        return $role->id;
    }

    public function get_role_user(){
        $role = Role::where('name','user')->first();
        return $role->id;
    }

    public static function boot()
    {
        parent::boot();
        static::creating(function($model){
            $model->role_id = $model->get_role_user();
        });
    }
}
