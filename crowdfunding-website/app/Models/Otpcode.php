<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid;

class Otpcode extends Model
{
    use HasFactory,Uuid;
    protected $fillable = [
        'otp',
        'valid_until',
        'user_id'
    ];
    protected $primaryKey = 'id';
    

    public function user(){
        return $this->belongsTo('App\User');
    }
}
