<?php

class Hewan {

    public $nama;
    public $darah = 50;
    public $jumlahKaki;
    public $keahlian;

    public function atraksi(){
        return "$this->nama, $this->keahlian";
    }
}

class Fight{

    public $attackPower;
    public $defencePower;

    public function serang(){
        return "__";
    }

    public function diserang(){
        return "darah sekarang - attackPower penyerang/defencePower yang diserang";
    }

}

class Elang{
    public function getInfoHewan(){ 
        return "
        $this->nama, 
        $this->darah,
        $this->jumlahKaki,
        $this->keahlian,
        $this->attackPower,
        $this->defencePower
        ";
    }
}

?>